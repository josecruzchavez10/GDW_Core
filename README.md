# Magento2 Extendable Shipping Save Processor

Because we need extensions to not conflict with each other.

## How to use

```javascript
// Your/Module/view/frontend/requirejs-config.js

var config = {
    "config": {
        "mixins": {
            "GDW_Core/js/extender-save-attributes": {
                "Your_Module/js/extender-save-attributes": true
            }
        }
    }
}
```

```javascript
// Your/Module/view/frontend/web/extender.js

define(['mage/utils/wrapper'], function (wrapper) {
    'use strict';
    
    return function (processor) {
        return wrapper.wrap(processor, function (proceed, payload) {
            payload = proceed(payload);
            payload.addressInformation.extension_attributes.yourAttribute = 'value';
            return payload;
        });
    };
});
```

## But why put it on github?

Easier installation across our multiple implementations.